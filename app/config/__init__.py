
class BaseConfig:
    TESTING = False
    DEBUG = True
    LOGGER_NAME = 'app'
    LOG_FILE = 'app.log'

    REDIS_CONNECTION = {
        'host': 'localhost',
        'port': '6379',
        'db': 0
    }

    MYSQL_CONNECTION = {
        'host': 'localhost',
        'port': 3306,
        'user': 'root',
        'password': 'blah',
        'db': 'blah'
    }

    ERRORS = {
        'JSONParseError': {
            'message': 'whoa thats not JSON',
            'status': 504
        }
    }
