from app.config import BaseConfig


class TestConfig(BaseConfig):
    # TODO: fix this its not ideal to copy stuffs from BaseConfig
    REDIS_CONNECTION = BaseConfig.REDIS_CONNECTION
    REDIS_CONNECTION['db'] = 1

    MYSQL_CONNECTION = BaseConfig.MYSQL_CONNECTION
    MYSQL_CONNECTION['database'] = 'simpleToken_test'
    TESTING = True
