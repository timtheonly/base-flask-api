from flask import Flask
from flask_restful import Api

from app.bootstrap import load_routes
from app.DataAccess.config import get_current_config


application = Flask(__name__)

config = get_current_config()
application.config.from_object(config)

api = Api(application, errors=config.ERRORS)
api = load_routes(api)

if not config.TESTING:
    import logging
    file_handler = logging.FileHandler(config.LOG_FILE, mode='a')
    file_handler.setLevel(logging.WARNING)
    application.logger.addHandler(file_handler)


@application.errorhandler(Exception)
def error_handler(e):
    application.logger.warning("Exception: ".format(str(e)))
    return '{"message": "Internal Error", "status": 502}', 502
