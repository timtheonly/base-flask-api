from app.route import ResourceMixin
from flask_restful import Resource


class Example(Resource, ResourceMixin):
    available_routes = ['/example']

    def get(self):
        pass

    def post(self):
        pass

    def put(self):
        pass

    def delete(self):
        pass
