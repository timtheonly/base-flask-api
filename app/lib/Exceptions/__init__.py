from flask_restful import HTTPException


class JSONParseError(HTTPException):
    pass
