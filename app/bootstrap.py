from app.route.example import Example


def load_routes(api):

    views = [Example]
    for view in views:
        api.add_resource(view, *view.available_routes)

    return api
