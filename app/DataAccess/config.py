import os
from app.config import BaseConfig
from app.config.devConfig import DevConfig
from app.config.testConfig import TestConfig


def get_current_config():
    config = BaseConfig()
    if os.getenv('ENV') == 'DEV':
        config = DevConfig()
    elif os.getenv('ENV') == 'TEST':
        config = TestConfig()
    return config
