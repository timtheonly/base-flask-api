# Flask-Base
Restful flask boilerplate.

## Getting started
1. Add your custom route to `/yourapplication/route/my_new_route.py`. Take a look at [Example.py](/app/route/example.py), as a starting place.
2. Lets tell flask to use your new route by adding it to `/yourapplication/bootstrap.py`. It should look something like this:

```python
from app.route.example import Example
from app.route.my_new_route import NewRoute


def load_routes(api):
    views = [Example, NewRoute]
    for view in views:
        api.add_resource(view, *view.available_routes)
    return api
```
3. Run using `run.py`, your app should be running on [localhost:8000](http://localhost:8000/)

## Project layout

````
/yourapplication
    __init__.py
    server.py           <<- core flask logic
    bootstrap.py        <<- bootstraps routes
    /config
        __init__.py     <<- base config
        devConfig.py
        testConfig.py
    /dataAccess         <<- data layer
        __init__.py
        ....
    /lib                <<- pull logic from routes into libs
        /Exceptions
            __init__.py <<- define custom exceptions
        __init__.py
        ....
    /route              <<- custom routes go here
        __init__.py
        example.py
```
