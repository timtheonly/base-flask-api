import os
import json
import unittest
from nose.tools import *


@unittest.skipUnless(os.environ.get("DB_HITS"), 1)
class TestApp(unittest.TestCase):
    """
    fix these tests we shouldn't be hitting the database at all or at the very
    least cleaning up the database after we're finished
    """
    def setUp(self):
        from app.server import application
        self.testapp = application.test_client()

    def test_index(self):
        """Receives correct error for incorrect route"""
        response = self.testapp.get('/')
        assert_equals(response.status_code, 404)
        assert_true("Not Found" in response.data)
